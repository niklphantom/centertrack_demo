## Демо

Демо обнаружения и сопровождения автомобилей с помощью нейросети CenterTrack.

<p align="center"> <img src='car_tracking.gif' align="center" height="230px"> </p>


[Скачать](https://drive.google.com/drive/folders/1VYSh3DZn8Qmpwjb5cB-zB_9x2FfHAJvv?usp=sharing) использованные предобученные веса, исходное (4k 60fps 30s) и обработанное (4k 30 fps 60s) видео, json с результатами.

## Установка

1. Создание виртуальной среды anaconda
    ~~~
    conda create --name <env> --file requirements.txt
    ~~~

2. 
    ~~~
    conda activate <env>
    ~~~
3. Склонировать репозиторий:

    ~~~
    CenterTrack_ROOT=/path/to/clone/CenterTrack
    git clone --recursive https://gitlab.com/niklphantom/centertrack_demo $CenterTrack_ROOT
    ~~~
    
4. Собрать Deformable convolutions ( [DCNv2](https://github.com/CharlesShang/DCNv2/)).

    ~~~
    cd $CenterTrack_ROOT/src/lib/model/networks/
    # git clone https://github.com/CharlesShang/DCNv2/ # clone if it is not automatically downloaded by `--recursive`.
    cd DCNv2
    ./make.sh
    ~~~

## Использование


`python demo.py tracking --load_model ../models/coco_tracking.pth --demo /home/user/Downloads/output.mp4 --save_results --save_video --only_cars --reformat --video_w 3864 --video_h 2192`

Результат предсказаний сохарняется в виде json следующеего вида:

```
{
  "frame_1":[{"x_left":10, "y_top":10, "x_right":20,"y_bottom":30, "tracking_id": 1}],
  "frame_2":
  [
      {"x_left":15, "y_top":15, "x_right":25,"y_bottom":35, "tracking_id": 1},
      {"x_left":35, "y_top":35, "x_right":65,"y_bottom":65, "tracking_id": 2}
  ]
  ...
}
```

## Источники

Оригинальный репозиторий CenterTrack:
https://github.com/xingyizhou/CenterTrack  

Статья:

> [**Tracking Objects as Points**](http://arxiv.org/abs/2004.01177),            
> Xingyi Zhou, Vladlen Koltun, Philipp Kr&auml;henb&uuml;hl,        
> *arXiv technical report ([arXiv 2004.01177](http://arxiv.org/abs/2004.01177))*  


